//
//  User.swift
//  Stindy
//
//  Created by Nguyễn Chí Thành on 6/20/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Realm

class User: Object {
    @objc dynamic var phoneNumber = ""
    @objc dynamic var fullName = ""
    @objc dynamic var created = Date()
    @objc dynamic var type = 0
    @objc dynamic var access_token = ""
    @objc dynamic var ID = ""
    @objc dynamic var isCurrent = false
    
    func setUser(JSON: JSON){
        self.phoneNumber = JSON[K_PhoneNumber].string ?? ""
        self.fullName = JSON[K_FullName].string ?? ""
        self.created = Date(fromString: JSON[K_FullName].string ?? "", format: .isoDateTimeMilliSec) ?? Date()
        self.type = JSON[K_Type].int ?? 0
        self.access_token = JSON[K_AccessToken].string ?? ""
        self.ID = JSON[K_UsersID].string ?? ""
    }
    func setCurrentUser(JSON: JSON){
        self.setUser(JSON: JSON)
        self.isCurrent = true
    }
}
