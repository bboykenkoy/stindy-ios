//
//  SignupViewController.swift
//  Stindy
//
//  Created by Lê Thị Lan Anh on 6/13/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController {

    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var rePassword: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    @IBAction func SIGNUP(_ sender: UIButton) {
        let params:[String: AnyObject] = [
            K_FullName: self.fullName.text as AnyObject,
            K_Password: self.password.text as AnyObject,
            K_PhoneNumber : self.phoneNumber.text as AnyObject
        ]
        if self.isValidate() {
            self.process()
            Service().signupWithAccount(info: params) { (JSON) in
                self.done()
                if JSON["status"].int == 200 {
                    self.show(string: JSON["message"].string ?? "")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.show(string: JSON["message"].string ?? "")
                }
            }
        }
    }
    // MARK: Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Đăng ký"
        self.isLightStatusBar = false
        self.navigationView.didClickLeftButton = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    // MARK: Settings
    func setupUI(){
        self.btnSignup.layer.cornerRadius = 5.0
        self.btnSignup.layer.masksToBounds = true
    }
    func isValidate() -> Bool {
        return true
    }
}
