//
//  LoginViewController.swift
//  Stindy
//
//  Created by Lê Thị Lan Anh on 6/13/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class LoginViewController: BaseViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBAction func LOGIN(_ sender: UIButton) {
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        let params:[String: AnyObject] = [
            K_Password: self.password.text as AnyObject,
            K_PhoneNumber : self.username.text as AnyObject,
            K_DeviceType: IOS as AnyObject,
            K_DeviceName: Device.name as AnyObject,
            K_DeviceVersion: Device.systemVersion as AnyObject,
            K_UUID: uuid as AnyObject
        ]
        if self.isValidate() {
            self.process()
            Service().signinWithAccount(info: params) { (JSON) in
                self.done()
                if JSON["status"].int == 200 {
                    let user = User()
                    user.setCurrentUser(JSON: JSON["message"])
                    App.shared.currentUser = user
                    App.shared.logged = true
                    let realm = try! Realm()
                    let logged = realm.objects(User.self).filter("isCurrent == true")
                    try! realm.write {
                        realm.delete(logged)
                        realm.add(user)
                    }
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.checkLogged()
                } else {
                    self.show(string: JSON["message"].string ?? "")
                }
            }
        }
    }
    @IBAction func SIGNUP(_ sender: UIButton) {
        let signup = SignupViewController(nibName: "SignupViewController", bundle: nil)
        self.navigationController?.pushViewController(signup, animated: true)
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    // MARK: Settings
    func setupUI(){
        self.btnLogin.layer.cornerRadius = 5.0
        self.btnLogin.layer.masksToBounds = true
    }
    func isValidate() -> Bool {
        return true
    }
}
