//
//  ContactCell.swift
//  Stindy
//
//  Created by Nguyễn Chí Thành on 6/24/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.layer.cornerRadius = 5.0
        self.bgView.layer.masksToBounds = true
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avatar.layer.cornerRadius = 55/2
        self.avatar.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
