//
//  ContactViewController.swift
//  Stindy
//
//  Created by Nguyễn Chí Thành on 6/23/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit

class ContactViewController: BaseViewController {
    @IBOutlet weak var bgTextfieldSearch: UIView!
    @IBOutlet weak var topSearcherConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    
    var isShowSearcher: Bool!{
        didSet{
            if isShowSearcher {
                UIView.animate(withDuration: 0.3) {
                    self.topSearcherConstraint.constant = HEADER_HEIGHT
                    self.view.layoutIfNeeded()
                    self.searchTextfield.becomeFirstResponder()
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.topSearcherConstraint.constant = HEADER_HEIGHT-55
                    self.view.layoutIfNeeded()
                    self.searchTextfield.resignFirstResponder()
                }
            }
        }
    }
    
    @IBOutlet weak var searchTextfield: UITextField!
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.bgTextfieldSearch.layer.cornerRadius = self.bgTextfieldSearch.frame.size.height/2
        self.bgTextfieldSearch.clipsToBounds = true
        
        if let clearButton = self.searchTextfield.value(forKey: "_clearButton") as? UIButton {
            let templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
            clearButton.setImage(templateImage, for: .normal)
            clearButton.tintColor = .darkGray
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topSearcherConstraint.constant = HEADER_HEIGHT-60
        self.view.layoutIfNeeded()
        self.isShowSearcher = false
        self.title = "Bạn bè"
        self.subTitle = "Tất cả 28 người"
        self.rightImage = UIImage(named: "ic_addFriend")
        self.leftImage = UIImage(named: "ic_search")
        self.navigationView.didClickRightButton = {
            
        }
        self.navigationView.didClickLeftButton = {
            self.isShowSearcher = !self.isShowSearcher
        }
        self.didTapSelfView = {
            self.isShowSearcher = false
        }
        //
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //
        
//        Service().friendRequest(friendsID: "5b2f2adf0c9947242cc5cc00") { (status) in
//            print(status)
//        }
        
        Service().acceptRequest(friendsID: "5b2f27880c9947242cc5cbfe") { (status) in
            print(status)
        }
    }
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier: String = "ContactCell"
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        let cell:ContactCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ContactCell
        if indexPath.row != 9 {
            cell.bottomConstraint.constant = 0
            cell.layoutIfNeeded()
        } else {
            cell.bottomConstraint.constant = 15
            cell.layoutIfNeeded()
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != 9 {
            return 100
        } else {
            return 115
        }
    }
}


