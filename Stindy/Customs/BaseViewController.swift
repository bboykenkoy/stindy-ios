//
//  BaseViewController.swift
//  Stindy
//
//  Created by Lê Thị Lan Anh on 6/13/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    typealias Callback = () -> Void
    var didTapSelfView: Callback!
    
    override var title: String?{
        didSet{
            if self.navigationView != nil {
                self.navigationView.title.text = self.title
            } else {
                self.isShowNavigationController = true
            }
        }
    }
    var subTitle: String!{
        didSet{
            if self.navigationView != nil {
                self.navigationView.subTitle.text = self.subTitle
                self.navigationView.centerTitleConstraint.isActive = false
                self.navigationView.subTitle.isHidden = false
            } else {
                self.isShowNavigationController = true
            }
        }
    }
    
    var isLightStatusBar: Bool! {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var isShowNavigationController: Bool! {
        didSet{
            if isShowNavigationController {
                self.initNavigationHeader()
            }
        }
    }
    
    private var hud: MBProgressHUD!
    var navigationView: StindyHeaderView!
    var rightImage: UIImage!{
        didSet{
            if isShowNavigationController {
                self.navigationView.imgRight.image = rightImage.withRenderingMode(.alwaysTemplate)
                self.navigationView.imgRight.tintColor = TINT_COLOR_IMAGE
            } else {
                self.initNavigationHeader()
                self.navigationView.imgRight.image = rightImage.withRenderingMode(.alwaysTemplate)
                self.navigationView.imgRight.tintColor = TINT_COLOR_IMAGE
            }
        }
    }
    var leftImage: UIImage!{
        didSet{
            if isShowNavigationController {
                self.navigationView.imgLeft.image = leftImage.withRenderingMode(.alwaysTemplate)
                self.navigationView.imgLeft.tintColor = TINT_COLOR_IMAGE
            } else {
                self.initNavigationHeader()
                self.navigationView.imgLeft.image = leftImage.withRenderingMode(.alwaysTemplate)
                self.navigationView.imgLeft.tintColor = TINT_COLOR_IMAGE
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return (self.isLightStatusBar != nil && self.isLightStatusBar) ? .lightContent : .default
    }
    
    
    func show(string: String = ""){
        var stringHud:MBProgressHUD!
        if (self.navigationController != nil){
            stringHud = MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
        } else {
            stringHud = MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        stringHud.mode = MBProgressHUDMode.text
        stringHud.animationType = .zoomIn
        stringHud.bezelView.backgroundColor = .white
        stringHud.backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.55)
        stringHud.label.text = string
        stringHud.hide(animated: true, afterDelay: 1.2)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            stringHud.removeFromSuperview()
        }
    }
    
    func process(){
        if (self.navigationController != nil){
            self.hud = MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
        } else {
            self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.animationType = .zoomIn
        self.hud.bezelView.backgroundColor = .white
        self.hud.backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.55)
    }
    func done(){
        self.hud.hide(animated: true)
        self.hud.removeFromSuperview()
    }
    
    
    // MARK: LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapDismiss))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    // MARK: NAVIGATION
    func initNavigationHeader(){
        if self.navigationView == nil {
            self.navigationView = StindyHeaderView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: HEADER_HEIGHT))
            self.navigationView.title.text = self.title
//            self.navigationView.layer.borderColor = UIColor.blue.cgColor
//            self.navigationView.layer.borderWidth = 1.0
            self.view.addSubview(self.navigationView)
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    
    
    @objc func tapDismiss(){
        self.view.endEditing(true)
        if self.didTapSelfView != nil {
            self.didTapSelfView()
        }
    }
}
