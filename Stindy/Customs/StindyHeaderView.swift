//
//  StindyHeaderView.swift
//  Stindy
//
//  Created by Nguyễn Chí Thành on 6/23/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit

class StindyHeaderView: UIView {

    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var imgRight: UIImageView!
    @IBOutlet weak var btnRight: UIButton!
    
    typealias Callback = () -> Void
    var didClickLeftButton: Callback!{
        didSet{
            self.imgLeft.isHidden = false
            let image = self.imgLeft.image
            self.imgLeft.image = image?.withRenderingMode(.alwaysTemplate)
            self.imgLeft.tintColor = TINT_COLOR_IMAGE
        }
    }
    var didClickRightButton: Callback!{
        didSet{
            self.imgRight.isHidden = false
            let image = self.imgRight.image
            self.imgRight.image = image?.withRenderingMode(.alwaysTemplate)
            self.imgRight.tintColor = TINT_COLOR_IMAGE
        }
    }
    @IBOutlet weak var centerTitleConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBAction func clickLeftButton(_ sender: UIButton) {
        if self.didClickLeftButton != nil {
            self.didClickLeftButton()
        }
    }
    @IBAction func clickRightButton(_ sender: UIButton) {
        if self.didClickRightButton != nil {
            self.didClickRightButton()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib(name: "StindyHeaderView")
        self.setupNormal()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func setupNormal(){
        self.imgLeft.isHidden = true
        self.imgRight.isHidden = true
        self.centerTitleConstraint.isActive = true
        self.subTitle.isHidden = true
    }
}
