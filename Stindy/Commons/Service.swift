//
//  Service.swift
//  Locate DiDi
//
//  Created by Nguyễn Chí Thành on 10/01/2018.
//  Copyright © 2018 Nguyễn Chí Thành. All rights reserved.
//
import UIKit
import SwiftyJSON
import Alamofire

class Service: BaseService {
    func signupWithAccount(info: [String: AnyObject], completion: @escaping (_ status: JSON) ->()){
        self.post(type: .API_SIGNUP, params: info) { (jsonData) in
            completion(jsonData)
        }
    }
    func signinWithAccount(info: [String: AnyObject], completion: @escaping (_ status: JSON) ->()){
        self.post(type: .API_SIGNIN, params: info) { (jsonData) in
            completion(jsonData)
        }
    }
    func auth(completion: @escaping (_ authenticated: Bool) ->()){
        self.post(type: .API_AUTH, params: [:]) { (jsonData) in
            if jsonData[K_Status].int == 200 {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func friendRequest(friendsID: String, completion: @escaping (_ status: JSON) ->()){
        let params: [String: AnyObject] = [K_FriendsID: friendsID as AnyObject]
        self.post(type: .API_FRIEND_REQUEST, params: params) { (jsonData) in
            completion(jsonData)
        }
    }
    
    func acceptRequest(friendsID: String, completion: @escaping (_ status: JSON) ->()){
        let params: [String: AnyObject] = [K_FriendsID: friendsID as AnyObject]
        self.post(type: .API_ACCEPT_REQUEST, params: params) { (jsonData) in
            completion(jsonData)
        }
    }
}
