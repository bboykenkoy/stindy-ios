//
//  ServiceManager
//  ChatApp
//
//  Created by Nguyễn Chí Thành on 02/01/2018.
//  Copyright © 2018 Nguyễn Chí Thành. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum API_SERVICE {
    case API_SIGNUP
    case API_SIGNIN
    case API_UPDATE
    case API_AUTH
    case API_FRIEND_REQUEST
    case API_ACCEPT_REQUEST
    case API_GET_CONVERSATIONS
    case API_GET_MESSAGES
    case API_LIST_ONLINE
    case API_GET_USER
    case API_GET_FRIEND
    case API_SIGNOUT
    case API_CREATE_CONVERSATION
}
let BASE : String = "http://192.168.1.65"
let BASE_URL : String = "\(BASE):3000/"
let SERVER_URL: String = "\(BASE):3000"


class BaseService: NSObject {
    
    static let shared = {
        return BaseService()
    }
    func getURLWithType(type : API_SERVICE, params : [String : AnyObject]) -> String{
        var result = String()
        switch type {
        case .API_AUTH:
            result = "\(BASE_URL)users/auth"
            break
        case .API_SIGNUP:
            result = "\(BASE_URL)users/signup"
            break
        case .API_SIGNIN:
            result = "\(BASE_URL)users/signin"
            break
        case .API_UPDATE:
            result = "\(BASE_URL)users/update"
            break
        case .API_CREATE_CONVERSATION:
            result = "\(BASE_URL)conversations/new"
            break
        case .API_FRIEND_REQUEST:
            result = "\(BASE_URL)users/friends/request"
            break;
        case .API_ACCEPT_REQUEST:
            result = "\(BASE_URL)users/friends/accept"
            break;
        default:
            result =  ""
            break
        }
        return result
    }
    func get(type : API_SERVICE , params : [String:AnyObject],completion: @escaping (_ response : JSON)->()){
        let urlRequest = getURLWithType(type: type,params: params)
        var headers: [String: String] = [:]
        if(App.shared.logged){
            headers["Authorization"] = "Bearer \(App.shared.currentUser.access_token)"
        }
        Alamofire.request(urlRequest, method: HTTPMethod.get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (dataResponse) in
            let json = SwiftyJSON.JSON(dataResponse.value ?? [:])
            completion(json)
        }
    }
    func post(type : API_SERVICE , params : [String:AnyObject],completion: @escaping (_ response : JSON)->()){
        let urlRequest = getURLWithType(type: type,params: params)
        var headers: [String: String] = [:]
        if(App.shared.logged){
            headers["Authorization"] = "Bearer \(App.shared.currentUser.access_token)"
        }
        Alamofire.request(urlRequest, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (dataResponse) in
            let json = SwiftyJSON.JSON(dataResponse.value ?? [:])
            completion(json)
        }
    }
    
}
