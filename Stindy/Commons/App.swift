//
//  AppManager.swift
//  Stindy
//
//  Created by Nguyễn Chí Thành on 6/20/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift

class App {
    static let shared = App()
    var currentUser: User!
    var logged: Bool!
    let realm = try! Realm()
    private init() {
        let logged = realm.objects(User.self).filter("isCurrent == true")
        if logged.count > 0 {
            self.logged = true
            self.currentUser = logged.first
        } else {
            self.logged = false
        }
    }
}
