//
//  Constants.swift
//  Stindy
//
//  Created by Lê Thị Lan Anh on 6/13/18.
//  Copyright © 2018 Lê Thị Lan Anh. All rights reserved.
//

import Foundation
import UIKit

let BASE_COLOR = UIColor(hex:"4C1364")
let HEADER_HEIGHT = CGFloat(80.0)
let TINT_COLOR_IMAGE = BASE_COLOR//UIColor.lightGray.withAlphaComponent(0.9)

// BASE
let K_Status = "status"
let K_Time = "time"
let K_Message = "message"
// USERS
let K_UsersID = "usersId"
let K_FriendsID = "friendsId"
let K_FullName = "fullName"
let K_Password = "password"
let K_PhoneNumber = "phoneNumber"
let K_Type = "type"
let K_DeviceType = "deviceType"
let K_DeviceToken = "deviceToken"
let K_DeviceName = "deviceName"
let K_DeviceVersion = "deviceVersion"
let K_AccessToken = "access_token"
let K_UUID = "uuid"
let IOS = "ios"
